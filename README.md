# Project BitMaxCC

## Log Rec

### Ver 1.0

* for 2021.5.26
* 建仓入仓

### Ver 1.1

* for 2021.5.27
* 加入 ./src/test 测试文件

### Ver 4.1

* for 2021.5.28
* 测试连接

### Ver 3.1

* for 2021.5.28
* 增加部分语义分析代码
* TIPS： NOT FINISHED

### Ver 2.1

* for 2021.5.28
* 更改词法分析.txt，内容是tokens和name的对应关系

### Ver 2.1.1

* for 2021.5.28
* 添加misc文件夹存储杂项
* 在misc文件夹中添加用户测试脚本，用于进行模块测试
* 在src文件夹中添加test2测试用例

### Ver 2.2

* for 2021.5.29
* misc中更新了测试脚本，具体使用说明见对应文件夹中的md文件
* scanner中添加scanner.cpp、.h和.exe文件，初步完成词法分析
* src文件夹中添加test_scanner、test2_scanner文件夹，作为测试用例对应的单元测试结果



### Ver 3.1

* for 2021.6.3
* 加入Clion项目，构建文法类