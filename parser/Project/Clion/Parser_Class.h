//
// Created by 李泽林 on 2021/6/3.
//
#include <cstdint>

#include <algorithm>
#include <vector>
#include <cstring>
#ifndef CLION_PARSER_CLASS_H
#define CLION_PARSER_CLASS_H

namespace ParserClass{
class CompUnit{
public:
    std::vector<CompUnit> compunit;
    int choose;
    Decl decl;
    FuncDef funcdef;
};

class Decl{
public:
    int choose;
    ConstDecl constdecl;
    VarDecl vardecl;
};

class ConstDecl{
public:
    BType btype;
    std::vector<ConstDef> constdef;
};

class BType{
public:
    std::string val;
};

class ConstDef{
public:
    std::vector<ConstExp> ConstExp;
    int constinitval;
};

class ConstInitVal{
public:

};

class VarDecl{
public:
    std::vector<VarDef> vardef;
};

class VarDef{
public:
    ConstExp constexp;
    InitVal InitVal;
};

class InitVal{
public:
    std::vector<Exp> exp;
};

class FuncDef{
public:
    FuncType Functype;
    FuncParams funcparams;
    Block block;
};

class FuncType{
public:
    std::string type;
};

class FuncFParams{
public:
    std::vector<FuncFParam> funcfparam;
};

class FuncFParam{
public:
    Ident ident;
    std::vector<EXP> exp;
};

class Block{
public:
    std::vector<BlockItem> blockitem;
};

class BlockItem{
public:
    int choose;
    Decl decl;
    Stme stmt;
};

class Stmt{
public:
    int choose;
    LVal lval;
    Exp exp;
    Block block;
    Cond cond;
    Stmt stmt;
    Stmt stmt2;
};

class Exp{
public:
    AddExp addexp;
};

class Cond{
public:
    LOrExp lorexp;
};

class LVal{
public:
    Ident ident;
    std::vector<Exp> exp;
};

class PrimaryExp{

};



}


#endif //CLION_PARSER_CLASS_H
