# 词法分析scanner

## 词法分析.txt
* 记录Token与对应word，以及代码实例

## scanner.h
* 头文件，声明了scanner.cpp中所需的类与函数

## scanner.cpp
* 将scanner.h中的文件进行实现
* 注意：main1函数为原模块中的main函数，如需单元测试，则将main1函数改为main即可测试

## scanner.exe
* 用于单元测试，具体使用方法参考misc文件夹中的README文件，使用“毕升代码工具”