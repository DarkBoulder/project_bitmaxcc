#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#ifndef SCANNER_H
#define SCANNER_H

using std::string;
using std::vector;

class Word {
public:
	string token_value;
	string word;
	unsigned int line_num;

	Word(string in_token_value, string in_word, unsigned int in_line_num) {
		token_value = in_token_value;
		word = in_word;
		line_num = in_line_num;
	}
};

void init_map();
string get_token(string& buf);
inline void get_char(char& ch, unsigned int& index, const string& str);
inline void retract(char& ch, unsigned int& index, const string& str);
inline bool is_octal_digit(char ch);
inline bool is_hex_digit(char ch);
inline bool is_nodigit(char ch);
inline bool is_illegal(char ch);
inline void word_scanner(string& input_str, vector<Word>& words);

#endif // !SCANNER_H
