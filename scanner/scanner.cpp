#define _CRT_SECURE_NO_WARNINGS
#include "scanner.h"

using namespace std;

unsigned int line_num = 1;
map<string, string> token_map;

inline void get_char(char& ch, unsigned int& index, const string& str) {
	if (index < str.size()) {
		ch = str[index];
		index++;
		if (ch == '\n') {
			line_num++;
		}
	}
}

void init_map() {
	token_map.insert({ "const", "CONST" });
	token_map.insert({ "int", "INT" });
	token_map.insert({ "void", "VOID" });
	token_map.insert({ "if", "IF" });
	token_map.insert({ "else", "ELSE" });
	token_map.insert({ "while", "WHILE" });
	token_map.insert({ "break", "BREAK" });
	token_map.insert({ "continue", "CONTINUE" });
	token_map.insert({ "return", "RETURN" });
}

string get_token(string& buf) {
	string tmp = buf;
	if (token_map.count(tmp) == 1) {
		return token_map[tmp];
	}
	else {
		return "IDENT";
	}
}

inline bool is_octal_digit(char ch) {
	return ch >= '0' && ch <= '7';
}

inline bool is_hex_digit(char ch) {
	return isdigit(ch) || (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F');
}

//回退，与get_char相反
inline void retract(char& ch, unsigned int& index, const string& str) {
	if (ch == '\n') {
		line_num--;
	}
	index--;
	ch = str[index - 1];
}

inline bool is_nodigit(char ch) { return isalpha(ch) || (ch == '_'); }

//不会出现的非法字符
inline bool is_illegal(char ch) {
	return (ch == '`' || ch == '~' || ch == '@' || ch == '#' || ch == '$' ||
		ch == '^') || (ch < 33 && ch > 126);
}

inline void word_scanner(string& input_str, vector<Word>& words) {
	string buf;
	unsigned int index = 0;
	char ch = 0;
	while (index < input_str.size()) {//一次循环读出一个词
		buf.clear();
		get_char(ch, index, input_str);
		while (is_illegal(ch) && index < input_str.size()) {//往后读直到读到合法字符
			get_char(ch, index, input_str);
		}//退出的可能性：非法字符到尾||合法字符未到尾||合法字符到尾
		if (is_illegal(ch)) {
			break;
		}
		else if (is_nodigit(ch)) {//保留词或标识符
			do {
				buf += ch;
				get_char(ch, index, input_str);
			} while ((is_nodigit(ch) || isdigit(ch)) && index < input_str.size());
			if (!(is_nodigit(ch) || isdigit(ch))) {
				retract(ch, index, input_str);
			}
			words.push_back(Word(get_token(buf), buf, line_num));
		}
		else if (isdigit(ch)) {//int
			if (ch != '0') {//10进制
				do {
					buf += ch;
					get_char(ch, index, input_str);
				} while (isdigit(ch) && index < input_str.size());
				if (!isdigit(ch)) {
					retract(ch, index, input_str);
				}
			}
			else {//8或16进制
				buf += ch;
				get_char(ch, index, input_str);
				if (ch == 'x' || ch == 'X') {//16
					buf += ch;
					get_char(ch, index, input_str);
					do {
						buf += ch;
						get_char(ch, index, input_str);
					} while (is_hex_digit(ch) && index < input_str.size());
					if (!is_hex_digit(ch)) {
						retract(ch, index, input_str);
					}
				}
				else if (is_octal_digit(ch)) {//8
					do {
						buf += ch;
						get_char(ch, index, input_str);
					} while (isdigit(ch) && index < input_str.size());
					if (!isdigit(ch)) {
						retract(ch, index, input_str);
					}
				}
				else {
					retract(ch, index, input_str);
				}
			}
			words.push_back(Word("INTC", buf, line_num));
		}
		else if (ch == '\"') {//string
			get_char(ch, index, input_str);
			while (ch != '\"') {
				buf += ch;
				get_char(ch, index, input_str);
			}
			words.push_back(Word("STR", buf, line_num));
		}
		else if (ch == '+') {
			buf += ch;
			words.push_back(Word("+", buf, line_num));
		}
		else if (ch == '-') {
			buf += ch;
			words.push_back(Word("-", buf, line_num));
		}
		else if (ch == '*') {
			buf += ch;
			words.push_back(Word("*", buf, line_num));
		}
		else if (ch == '/') {
			buf += ch;
			get_char(ch, index, input_str);
			if (ch == '/') {//单行注释
				do {
					get_char(ch, index, input_str);
				} while (ch != '\n' && index < input_str.size());
			}
			else if (ch == '*') {//多行注释
				bool rcv_star = false;//上一个是否是*
				do {
					get_char(ch, index, input_str);
					if (ch == '*') {
						rcv_star = true;
					}
					else if (ch == '/' && rcv_star) {
						break;
					}
					else {
						rcv_star = false;
					}
				} while (true);
			}
			else {
				retract(ch, index, input_str);
				words.push_back(Word("/", buf, line_num));
			}
		}
		else if (ch == '%') {
			buf += ch;
			words.push_back(Word("%", buf, line_num));
		}
		else if (ch == '=') {
			buf += ch;
			get_char(ch, index, input_str);
			if (ch == '=') {
				buf += ch;
				words.push_back(Word("==", buf, line_num));
			}
			else {
				retract(ch, index, input_str);
				words.push_back(Word("=", buf, line_num));
			}
		}
		else if (ch == '<') {
			buf += ch;
			get_char(ch, index, input_str);
			if (ch == '=') {
				buf += ch;
				words.push_back(Word("<=", buf, line_num));
			}
			else {
				retract(ch, index, input_str);
				words.push_back(Word("<", buf, line_num));
			}
		}
		else if (ch == '>') {
			buf += ch;
			get_char(ch, index, input_str);
			if (ch == '=') {
				buf += ch;
				words.push_back(Word(">=", buf, line_num));
			}
			else {
				retract(ch, index, input_str);
				words.push_back(Word(">", buf, line_num));
			}
		}
		else if (ch == '!') {
			buf += ch;
			get_char(ch, index, input_str);
			if (ch == '=') {
				buf += ch;
				words.push_back(Word("!=", buf, line_num));
			}
			else {
				retract(ch, index, input_str);
				words.push_back(Word("!", buf, line_num));
			}
		}
		else if (ch == '&') {
			buf += ch;
			get_char(ch, index, input_str);
			if (ch == '&') {
				buf += ch;
				words.push_back(Word("&&", buf, line_num));
			}
			else {
				retract(ch, index, input_str);
			}
		}
		else if (ch == '|') {
			buf += ch;
			get_char(ch, index, input_str);
			if (ch == '|') {
				buf += ch;
				words.push_back(Word("||", buf, line_num));
			}
			else {
				retract(ch, index, input_str);
			}
		}
		else if (ch == ';') {
			buf += ch;
			words.push_back(Word(";", buf, line_num));
		}
		else if (ch == ',') {
			buf += ch;
			words.push_back(Word(",", buf, line_num));
		}
		else if (ch == '(') {
			buf += ch;
			words.push_back(Word("(", buf, line_num));
		}
		else if (ch == ')') {
			buf += ch;
			words.push_back(Word(")", buf, line_num));
		}
		else if (ch == '[') {
			buf += ch;
			words.push_back(Word("[", buf, line_num));
		}
		else if (ch == ']') {
			buf += ch;
			words.push_back(Word("]", buf, line_num));
		}
		else if (ch == '{') {
			buf += ch;
			words.push_back(Word("{", buf, line_num));
		}
		else if (ch == '}') {
			buf += ch;
			words.push_back(Word("}", buf, line_num));
		}
	}
}

int main1(int argc, char* argv[]) {
	if (argc == 3) {
		freopen(argv[1], "r", stdin);
		freopen(argv[2], "w", stdout);
	}
	init_map();
	char ch;
	int count = 0;
	string input_str;
	vector<Word> words;
	while (scanf("%c", &ch) != EOF) {
		input_str += ch;
	}
	word_scanner(input_str, words);
	for (unsigned int i = 0; i < words.size(); i++) {
		cout << words[i].token_value << " " << words[i].word << " " << words[i].line_num << endl;
	}
	return 0;
}
